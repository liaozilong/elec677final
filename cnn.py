import pickle
import numpy as np
np.random.seed(1337)  # for reproducibility

from keras.models import Sequential, Model
from keras.layers import Input, Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from keras import backend as K
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adagrad, Adadelta, Adam, SGD
from keras.layers import Merge, merge
from keras.layers.convolutional import ZeroPadding2D


batch_size = 100
nb_classes = 7
nb_epoch = 200
dropout_rate = 0.5

# input image dimensions
img_rows, img_cols = 48, 48
# number of convolutional filters to use
nb_filters = 64


# the data, shuffled and split between train and test sets
(training_data, test_data) = pickle.load(open('cohn_dataset.p','rb'))
(X_train, y_train), (X_test, y_test) = (training_data[0],training_data[1]),(test_data[0],test_data[1])

#ckecks if backend is theano or tensorflow for dataset format
if K.image_dim_ordering() == 'th':
    X_train = X_train.reshape(X_train.shape[0], 1, img_rows, img_cols)
    X_test = X_test.reshape(X_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    X_train = X_train.reshape(X_train.shape[0], img_rows, img_cols, 1)
    X_test = X_test.reshape(X_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')


# convert class vectors to binary class matrices
Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)
print(Y_train.shape)
print(Y_test.shape)


input = Input(shape=input_shape)
conv1_7x7_s2 = Convolution2D(nb_filters, 7, 7,
              activation='relu',border_mode='valid',
              subsample=(2,2), name='con1_7x7_s2')(input)
conv1_zero_pad = ZeroPadding2D(padding=(3,3), name='conv1_zero_pad')(conv1_7x7_s2)
pool1_3x3_s2 = MaxPooling2D(pool_size=(3,3), strides=(2,2),
                            name='pool1_3x3_s2')(conv1_zero_pad)
pool1_norm = BatchNormalization(name='pool1_norm')(pool1_3x3_s2)
pool1_dropout = Dropout(dropout_rate, name='pool1_dropout')(pool1_norm)


#separate
conv2a_1x1_s1 = Convolution2D(96, 1, 1, activation='relu',
                             subsample=(1,1), name='conv2a_1x1_s1')(pool1_dropout)
conv2b_3x3_s1 = Convolution2D(208, 3, 3, activation='relu',
                             subsample=(1,1), name='conv2b_3x3_s1')(conv2a_1x1_s1)
conv2b_zero_pad = ZeroPadding2D(padding=(1,1), name='conv2b_zero_pad')(conv2b_3x3_s1)


pool2a_3x3_s1 = MaxPooling2D(pool_size=(3,3), strides=(1,1), 
                             name='pool_2a_3x3_s1')(pool1_dropout)
pool2a_zero_pad = ZeroPadding2D(padding=(1,1), name='pool2a_zero_pad')(pool2a_3x3_s1)
conv2c_1x1_s1 = Convolution2D(nb_filters, 1, 1, activation='relu',
                             subsample=(1,1), name='conv2c_1x1_s1')(pool2a_zero_pad)


#merge first time
concat1 = merge([conv2b_zero_pad, conv2c_1x1_s1],mode='concat',
                concat_axis=1, name='concat1')
pool2b_3x3_s2 = MaxPooling2D(pool_size=(3,3), strides=(2,2),
                             name='pool2b_3x3_s2')(concat1)
pool2b_norm = BatchNormalization(name='pool2b_norm')(pool2b_3x3_s2)
pool2b_dropout = Dropout(dropout_rate, name='pool2b_dropout')(pool2b_norm)



#separate again
conv3a_1x1_s1 = Convolution2D(96, 1, 1, activation='relu',
                             subsample=(1,1), name='conv3a_1x1_s1')(pool2b_dropout)
conv3b_3x3_s1 = Convolution2D(208, 3, 3, activation='relu',
                             subsample=(1,1), name='coonv3b_3x3_s1')(conv3a_1x1_s1)
conv3b_zero_pad = ZeroPadding2D(padding=(1,1), name='conv3b_zero_pad')(conv3b_3x3_s1)


pool3a_3x3_s1 = MaxPooling2D(pool_size=(3,3), strides=(1,1),
                             name='pool3a_3x3_s1')(conv3b_zero_pad)
pool3a_zero_pad = ZeroPadding2D(padding=(1,1), name='pool3a_zero_pad')(pool3a_3x3_s1)
conv3c_1x1_s1 = Convolution2D(nb_filters, 1,1, activation='relu',
                             subsample=(1,1), name='conv3c_1x1_s1')(pool3a_zero_pad)



#merge second time
concat2 = merge([conv3b_zero_pad, conv3c_1x1_s1],mode='concat',
                concat_axis=1, name='concat2')
pool3b = MaxPooling2D(pool_size=(3,3), strides=(2,2), name='pool3b')(concat2)


#A Fully Conntected Layer with relu and a output layer with softmax
flat = Flatten()(pool3b)
fc1 = Dense(1024, activation='relu')(flat)
fc1_dropout = Dropout(dropout_rate)(fc1)
classifier = Dense(nb_classes)(fc1_dropout)
classifier_act = Activation('softmax')(classifier)


model = Model(input=input, output=classifier_act)
#compiling the model 
adam = Adam(lr=0.001, beta_1=0.9,beta_2=0.999,epsilon=1e-08)
model.compile(loss='categorical_crossentropy',
              optimizer=adam,
              metrics=['accuracy'])


model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch, verbose=1, validation_data=(X_test, Y_test))
score = model.evaluate(X_test, Y_test, verbose=0)

print('Test score:', score[0])
print('Test accuracy:', score[1])


# Plotting functions
## from https://gist.github.com/hadim/9fedb72b54eb3bc453362274cd347a6a#file-keras_plot-ipynb
## make_mosaic(im, nrows, ncols, border=1)
## Create a numpy mosaic from a list of matrix.

## get_weights_mosaic(model, layer_id, n=64)
## Get the weights of the layer of a model as a numpy mosaic.

## plot_weights(model, layer_id, n=64, ax=None, **kwargs)
## Plot the weights of a specific layer with matplotlib

## plot_all_weights(model, n=64, **kwargs)
## Plot all the possible 2D weights in the model

## plot_feature_map(model, layer_id, X, n=256, ax=None, **kwargs)
## Plot the feature maps of the layer of a model

## plot_all_feature_maps(model, X, n=256, ax=None, **kwargs)
## Plot all the feature maps of every possible layers 

def make_mosaic(im, nrows, ncols, border=1):
    """From http://nbviewer.jupyter.org/github/julienr/ipynb_playground/blob/master/keras/convmnist/keras_cnn_mnist.ipynb
    """
    import numpy.ma as ma

    nimgs = len(im)
    imshape = im[0].shape
    
    mosaic = ma.masked_all((nrows * imshape[0] + (nrows - 1) * border,
                            ncols * imshape[1] + (ncols - 1) * border),
                            dtype=np.float32)
    
    paddedh = imshape[0] + border
    paddedw = imshape[1] + border
    im
    for i in range(nimgs):
        
        row = int(np.floor(i / ncols))
        col = i % ncols
        
        mosaic[row * paddedh:row * paddedh + imshape[0],
               col * paddedw:col * paddedw + imshape[1]] = im[i]
        
    return mosaic


def get_weights_mosaic(model, layer_id, n=64):
    """
    """
    
    # Get Keras layer
    layer = model.layers[layer_id]

    # Check if this layer has weight values
    if not hasattr(layer, "W"):
        raise Exception("The layer {} of type {} does not have weights.".format(layer.name,
                                                           layer.__class__.__name__))
        
    weights = layer.W.get_value()
    
    # For now we only handle Conv layer like with 4 dimensions
    if weights.ndim != 4:
        raise Exception("The layer {} has {} dimensions which is not supported.".format(layer.name, weights.ndim))
    
    # n define the maximum number of weights to display
    if weights.shape[0] < n:
        n = weights.shape[0]
        
    # Create the mosaic of weights
    nrows = int(np.round(np.sqrt(n)))
    ncols = int(nrows)

    if nrows ** 2 < n:
        ncols +=1

    mosaic = make_mosaic(weights[:n, 0], nrows, ncols, border=1)
    
    return mosaic


def plot_weights(model, layer_id, n=64, ax=None, **kwargs):
    """Plot the weights of a specific layer. ndim must be 4.
    """
    import matplotlib.pyplot as plt
    
    # Set default matplotlib parameters
    if not 'interpolation' in kwargs.keys():
        kwargs['interpolation'] = "none"
        
    if not 'cmap' in kwargs.keys():
        kwargs['cmap'] = "gray"
    
    layer = model.layers[layer_id]
    
    mosaic = get_weights_mosaic(model, layer_id, n=64)
    
    # Plot the mosaic
    if not ax:
        fig = plt.figure()
        ax = plt.subplot()
    
    im = ax.imshow(mosaic, **kwargs)
    ax.set_title("Layer #{} called '{}' of type {}".format(layer_id, layer.name, layer.__class__.__name__))
    
    plt.colorbar(im, ax=ax)
    
    return ax


def plot_all_weights(model, n=64, **kwargs):
    """
    """
    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    
    # Set default matplotlib parameters
    if not 'interpolation' in kwargs.keys():
        kwargs['interpolation'] = "none"

    if not 'cmap' in kwargs.keys():
        kwargs['cmap'] = "gray"

    layers_to_show = []

    for i, layer in enumerate(model.layers):
        if hasattr(layer, "W"):
            weights = layer.W.get_value()
            if weights.ndim == 4:
                layers_to_show.append((i, layer))


    fig = plt.figure(figsize=(15, 15))
    
    n_mosaic = len(layers_to_show)
    nrows = int(np.round(np.sqrt(n_mosaic)))
    ncols = int(nrows)

    if nrows ** 2 < n_mosaic:
        ncols +=1

    for i, (layer_id, layer) in enumerate(layers_to_show):

        mosaic = get_weights_mosaic(model, layer_id=layer_id, n=n)

        ax = fig.add_subplot(nrows, ncols, i+1)
        
        im = ax.imshow(mosaic, **kwargs)
        ax.set_title("Layer #{} called '{}' of type {}".format(layer_id, layer.name, layer.__class__.__name__))

        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.1)
        plt.colorbar(im, cax=cax)
        
    fig.tight_layout()
    return fig


def plot_feature_map(model, layer_id, X, n=512, ax=None, **kwargs):
    """
    """
    import keras.backend as K
    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid1 import make_axes_locatable

    layer = model.layers[layer_id]
    
    try:
        get_activations = K.function([model.layers[0].input, K.learning_phase()], [layer.output,])
        activations = get_activations([X, 0])[0]
    except:
        # Ugly catch, a cleaner logic is welcome here.
        raise Exception("This layer cannot be plotted.")
        
    # For now we only handle feature map with 4 dimensions
    if activations.ndim != 4:
        raise Exception("Feature map of '{}' has {} dimensions which is not supported.".format(layer.name,
                                                                                             activations.ndim))
        
    # Set default matplotlib parameters
    if not 'interpolation' in kwargs.keys():
        kwargs['interpolation'] = "none"

    if not 'cmap' in kwargs.keys():
        kwargs['cmap'] = "gray"
        
    fig = plt.figure(figsize=(15, 15))
    
    # Compute nrows and ncols for images
    n_mosaic = len(activations)
    nrows = int(np.round(np.sqrt(n_mosaic)))
    ncols = int(nrows)
    if (nrows ** 2) < n_mosaic:
        ncols +=1
        
    # Compute nrows and ncols for mosaics
    if activations[0].shape[0] < n:
        n = activations[0].shape[0]
        
    nrows_inside_mosaic = int(np.round(np.sqrt(n)))
    ncols_inside_mosaic = int(nrows_inside_mosaic)

    if nrows_inside_mosaic ** 2 < n:
        ncols_inside_mosaic += 1

    for i, feature_map in enumerate(activations):

        mosaic = make_mosaic(feature_map[:n], nrows_inside_mosaic, ncols_inside_mosaic, border=1)

        ax = fig.add_subplot(nrows, ncols, i+1)
        
        im = ax.imshow(mosaic, **kwargs)
        ax.set_title("Feature map #{} \nof layer#{} \ncalled '{}' \nof type {} ".format(i, layer_id,
                                                                                  layer.name,
                                                                                  layer.__class__.__name__))

        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.1)
        plt.colorbar(im, cax=cax)
            
    fig.tight_layout()
    plt.show()
    return fig


def plot_all_feature_maps(model, X, n=512, ax=None, **kwargs):
    """
    """
    
    figs = []
    
    for i, layer in enumerate(model.layers):
        
        try:
            fig = plot_feature_map(model, i, X, n=n, ax=ax, **kwargs)
        except:
            print('failed')
            pass
        else:
            figs.append(fig)
    print(len(figs))        
    return figs

#plot_all_weights(model, n=256)


# Plot all the feature maps of the layer 2
# The maximum number of filters per feature maps is n=9


plot_all_feature_maps(model, X_test[:1], n=9)




